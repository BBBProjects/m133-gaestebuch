/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.gaestebuch;

import lombok.Getter;

/**
 *
 * @author Elia Fry
 */
public class User {

	@Getter
	private final int id;
	@Getter
	private final String name;

	public User(String name, int id) {
		this.name = name;
		this.id = id;
	}
}
