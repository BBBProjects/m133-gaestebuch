/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.gaestebuch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Alexander Flick
 */
public class LoginDAO_ {

	private XMLOutputter xmlOutput = new XMLOutputter();
	private SAXBuilder builder;
	private File xmlUsers;
	private File xmlBook;

	public LoginDAO_() {
		String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		String userPath = path + "WEB-INF/Userlist.xml";
		String bookPath = path + "WEB-INF/Book.xml";
		xmlUsers = new File(userPath);
		xmlBook = new File(bookPath);
		builder = new SAXBuilder();
	}

	public User check(String username, String password) {
		try {
			Document document = (Document) builder.build(xmlUsers);
			Element rootNode = document.getRootElement();
			List list = rootNode.getChildren("user");
			for (int i = 0; i < list.size(); i++) {
				Element node = (Element) list.get(i);
				if (node.getChildText("name").equals(username) && node.getChildText("password").equals(password)) {

					return new User(username, Integer.parseInt(node.getChildText("id")));
				}
			}
		} catch (JDOMException ex) {
			Logger.getLogger(LoginDAO_.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(LoginDAO_.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println("flop");
		return null;
	}

	public List<Message> getMessages() {
		try {
			Document document = (Document) builder.build(xmlBook);
			Element rootNode = document.getRootElement();
			List list = rootNode.getChildren("message");
			List<Message> messages = new ArrayList<>();
			for (int i = 0; i < list.size(); i++) {
				Element node = (Element) list.get(i);
				String rawTime = node.getChildText("time");
				//long timeMs = Long.parseLong(node.getChildText("time")) * 1000;
				//String timeDate = df.format(new Date(timeMs));

				messages.add(new Message(Integer.parseInt(node.getChildText("id")), node.getChildText("name"), rawTime, node.getChildText("message")));
			}
			return messages;
		} catch (JDOMException | IOException ex) {
			Logger.getLogger(LoginDAO_.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println("flop");
		return null;
	}

	public void setEintrag(String name, String message) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		List<Message> lstMessages = getMessages();
		String id = String.valueOf(
			lstMessages.get(
			lstMessages.size()-1
		).getId() + 1);
		String time = df.format(new Date());
		//lstMessages.add(new Message(+1, name, df.format(new Date()), message));

		try {
			Element elMessage = new Element("message");
			Document document = (Document) builder.build(xmlBook);
			Element rootNode = document.getRootElement();

			elMessage.addContent(new Element("id").addContent(id));
			elMessage.addContent(new Element("name").addContent(name));
			elMessage.addContent(new Element("time").addContent(time));
			elMessage.addContent(new Element("message").addContent(message));

			rootNode.addContent(elMessage);

			//document = new Document(rootNode);
			xmlOutput.output(document, new FileWriter(xmlBook));System.out.println(xmlBook);

		} catch (JDOMException | IOException ex) {
			Logger.getLogger(LoginDAO_.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
