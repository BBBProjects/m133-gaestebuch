/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.gaestebuch;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Elia Fry
 */
@SessionScoped
@Named
public class LoginUserBean implements Serializable {

	LoginDAO_ log = new LoginDAO_();

	@Getter@Setter@Size(min = 1, message = "Please enter your Username")
	private String username;
	@Getter@Setter@Size(min = 1, message = "Please enter your Password")
	private String password;
	@Getter
	private boolean loggedIn;
	private User user;
	@Getter
	private List<Message> lstMessages;
	@Setter@Getter@Size(min = 1, message = "Please enter a Message")
	private String message;

	public LoginUserBean() {
		lstMessages = log.getMessages();
	}

	public String doLogin() {
		user = log.check(username, password);
		if (user != null) {
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			externalContext.getSessionMap().put("loginUserBean", user);
			return "secured/welcome?faces-redirect=true";
		}
		/*if ("admin".equals(username) && "Password1".equals(password)) {
			loggedIn = true;
			((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).changeSessionId();
			return "secured/welcome?faces-redirect=true";

		}*/

		return "start";
	}

	public String doLogout() {
		user = null;
		loggedIn = false;
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		externalContext.invalidateSession();
		/*
		//Controller erzeugen
		LoginUserBean login = new LoginUserBean();
		//vorherige Daten (notwendige) übertragen …
		//Session anlegen und Zugriffsname (hier log) festlegen
		externalContext.getSessionMap().put("log", login);*/
		return "/faces/start";
	}

	public String getSession() {
		FacesContext fCtx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		session.getId();
		return session.getId();
	}
	
	public void eintragen() {
		log.setEintrag(username, message);
	}
}
