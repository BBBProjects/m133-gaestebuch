/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.gaestebuch;

import java.util.Date;
import lombok.Getter;

/**
 *
 * @author Elia Fry
 */
public class Message {
	
	@Getter
	private final int id;
	@Getter
	private final String name;
	@Getter
	private final String time;
	@Getter
	private final String message;

	public Message(int id, String name, String time, String message) {
		this.id = id;
		this.name = name;
		this.time = time;
		this.message = message;
	}
}
